// **************  Modules
var express = require('express');
var router = express.Router();

//**************  Controllers
var webController = require('../controllers/web_controller');
var actionController = require('../controllers/action_controller');

// ************** 
router.get("/", webController.index); 				// App Index

// ************** 
router.get('/menu/:type', actionController.list); 		// it receives the parameter to check the JSON file

// **************  Route Export
module.exports = router;