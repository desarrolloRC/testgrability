// Constructor of the Cat Class
function Cat(name) {
  
  this.name = name;
  this.actualCenter = 1;
  this.lastCenter = 1;
  this.time = 0;

}
// class methods

//Method to calculate a route from the point of start to N amount of jumps
Cat.prototype.calculateRoute = function(centers, jumps, type) {
	if (type == 1) {
		this.lastCenter = centers[this.actualCenter].secondRoad;
		this.time = 0;
		for (var i = 1; i <= jumps; i++) {
			var next = centers[this.actualCenter].nextCenter(this.lastCenter);
			this.time = parseInt(this.time) + parseInt(next[1]);
			this.lastCenter = this.actualCenter;
			this.actualCenter = next[0];
		}
	}else{
		  
		this.actualCenter = 1;
		this.lastCenter = centers[this.actualCenter].firstRoad;
		this.time = 0;
		for (var i = 1; i <= jumps; i++) {
			var next = centers[this.actualCenter].nextCenter(this.lastCenter);
			this.time = parseInt(this.time) + parseInt(next[1]);
			this.lastCenter = this.actualCenter;
			this.actualCenter = next[0];
		}
	}
	return [this.time,this.actualCenter];
};

//Method to calculate the time to get to the last center from a starting point
Cat.prototype.calculateRouteHome = function(centers, numberCenters, type, actualCenter) {
	this.actualCenter = actualCenter;
	if (type == 1) {
		this.lastCenter = centers[this.actualCenter].secondRoad;
		this.time = 0;
		while(true){
			if(numberCenters != this.actualCenter){
				var next = centers[this.actualCenter].nextCenter(this.lastCenter);
				this.time = parseInt(this.time) + parseInt(next[1]);
				this.lastCenter = this.actualCenter;
				this.actualCenter = next[0];
			}else{
				break;
			}
		}
	}else{
		this.lastCenter = centers[this.actualCenter].firstRoad;
		this.time = 0;
		while(true){
			if(numberCenters != this.actualCenter){
				var next = centers[this.actualCenter].nextCenter(this.lastCenter);
				this.time = parseInt(this.time) + parseInt(next[1]);
				this.lastCenter = this.actualCenter;
				this.actualCenter = next[0];
			}else{
				break;
			}
		}
	}
	return this.time;
};

//Method to calculate the full route through one of the directions (left or right)
Cat.prototype.calculateRouteFull = function(centers, jumps, type) {
	var centersNumber = centers.length;
	centersNumber --;

	if(type == 1){
		var timeArray = this.calculateRoute(centers, jumps, 1);
        var time_home_first_route1 = this.calculateRouteHome(centers, centersNumber, 1, timeArray[1]);
        var time_home_second_route1 = this.calculateRouteHome(centers, centersNumber, 2, timeArray[1]);
        var firstRouteTimeBC = timeArray[0] + Math.min(time_home_first_route1, time_home_second_route1);

	}else{
		var timeArray = this.calculateRoute(centers, jumps, 2);

        var time_home_first_route1 = this.calculateRouteHome(centers, centersNumber, 1, timeArray[1]);
        var time_home_second_route1 = this.calculateRouteHome(centers, centersNumber, 2, timeArray[1]);
        var firstRouteTimeBC = timeArray[0] + Math.min(time_home_first_route1, time_home_second_route1);
	}

	return firstRouteTimeBC;
};

//Method to calculate both routes (left or right) and get the one that takes the longest
Cat.prototype.getShortestRoute = function(centers, jumps) {
    var time_first_route = this.calculateRouteFull(centers, jumps, 1);
    var time_second_route = this.calculateRouteFull(centers, jumps, 2);

	return Math.min(time_first_route,time_second_route);
};
// export the class
module.exports = Cat;