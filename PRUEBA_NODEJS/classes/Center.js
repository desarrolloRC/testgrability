// Constructor for the Center Class
function Center(name) {

  this.name = name;
  this.fishToSell = 0;
  this.firstRoad = 0;
  this.firstRoadTime = 0;
  this.secondRoad = 0;
  this.secondRoadTime = 0;

}
// class methods}

//Methods to set the first unseted conector to the center, if the first position is taken it set the other one
Center.prototype.setfirstRoad = function(line) {
	if(this.firstRoad == 0){
		this.firstRoad = line[1];
		this.firstRoadTime = line[2];
	}else{
		this.secondRoad = line[1];
		this.secondRoadTime = line[2];
	}

};

Center.prototype.setSecondRoad = function(line) {

	if(this.firstRoad == 0){
		this.firstRoad = line[0];
		this.firstRoadTime = line[2];
	}else{
		this.secondRoad = line[0];
		this.secondRoadTime = line[2];
	}

};

//Method that receives the last center to return the next one to jump
Center.prototype.nextCenter = function(last) {

	if(this.firstRoad == last){
		return [this.secondRoad, this.secondRoadTime];
	}else{
		return [this.firstRoad, this.firstRoadTime];
	}

};
// export the class
module.exports = Center;