//Main Controller for the actions
var http = require('http'); //Module for the HTTP request
var Center = require('../classes/Center'); //Class Center
var Cat = require('../classes/Cat'); //Class Cat

//Function that depending on the request gets the right JSON file so it can calculate the time
exports.list = function (req, res){

	if(req.params.type == 1){
		var url = 'http://www.mocky.io/v2/5968f742110000180e614cd5';
	}else if(req.params.type == 2){
		var url = 'http://www.mocky.io/v2/59690f94110000250e614d3f';
	}else if(req.params.type == 3){
		var url = 'http://www.mocky.io/v2/59690f11110000440e614d3c';
	}
	

	http.get(url, function(response){
	    var body = '';

	    response.on('data', function(chunk){
	        body += chunk;
	    });

	    response.on('end', function(){
	        var Response = JSON.parse(body);
	        var lines = Response.lines.split("\n");
	        var centers = 0;
	        var roads = 0;
	        var fishes = 0;
	        var centersArray = [];

	        //Constructor for the centers and their roads to each one of them
	        for (var i = 0; i < lines.length; i++) {
	        	if(lines[i] != ''){
	        		var line = lines[i].split(' ');
	        		if(i==0){
	        			centers = line[0];
	        			roads = line[1];
	        			fishes = line[2];
		        		for (var p = 1; p <= centers; p++) {
		        			centersArray[p] = new Center('Center #'+p);
		        		}
	        		}else{
		        		if(line.length == 2){
		        			centersArray[line[1]].fishToSell = line[0];
		        		}else{
		        			centersArray[line[0]].setfirstRoad(line);
		        			centersArray[line[1]].setSecondRoad(line);
		        		}
	        		}
	        	}
	        }

	        //It divides the amount of fish to buy so it can asign them to each cat
	        var division = fishes / 2;
	        var division1 = Math.floor(division);
	        var division2 = Math.ceil(division);

	        var big_cat = new Cat('Big Cat');
	        var time_first_route_bc = big_cat.getShortestRoute(centersArray, division2);

	        var little_cat = new Cat('Little Cat');
	        var time_first_route_lc = little_cat.getShortestRoute(centersArray, division1);

	        //The json estructure is sent to the view with the longest time
    		res.render('action/index', {centersArray: lines , totalTime : Math.max(time_first_route_bc, time_first_route_lc) });
	        
	    });
	}).on('error', function(e){
	      console.log("Got an error: ", e);
	});

	
};